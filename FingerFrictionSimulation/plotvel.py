from matplotlib import pyplot
import math
import numpy as np
import matplotlib.pyplot as plt
import os
import glob


path1 = "proxyvel.txt"
with open(path1) as f:
	s = f.read()
	sim = s.split();
	sim = [float(s) for s in sim]

path2 = "InputData/Acril/AcrilVelocity.txt"
with open(path2) as f:
	s = f.read()
	v = s.split();
	v = [float(s) for s in v]

#プロット
plt.plot(sim)
plt.plot(v)
plt.show()
