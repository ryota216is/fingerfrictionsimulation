from matplotlib import pyplot
import math
import numpy as np
import matplotlib.pyplot as plt
import os

#ファイル読み出し
#name = 'Bakelite'
name = 'Acril'
path = 'InputData/' + name  + '/' + name + 'AccelTotal.txt'
with open(path) as f:
	s = f.read()
	a = s.split();
	a = [float(s) for s in a]

#ローパスフィルタ作成
from scipy import signal
lowpass = signal.firwin(numtaps=21, cutoff=200, fs=2000)	#カットオフ100Hz、サンプリング2kHz
lowlowpass = signal.firwin(numtaps=101, cutoff=20, fs=2000)	#カットオフ10Hz、サンプリング2kHz
#plt.plot(lowpass)
#plt.plot(lowlowpass)
#plt.show()
print(lowpass)

#ローパスフィルタをかける
y = signal.filtfilt(lowpass, 1, a)			# ローパス後の加速度
ave = signal.filtfilt(lowlowpass, 1, a)		# 0がずれるので、周囲の平均値として使う信号

#極大値を探すとともに、付近の加速度を積分して、滑りの加速度を求める
maximulTime = []	#極大値の時刻
maximulAccel = []	#対応する滑りの加速度
diff = 0.001
acc = 0 # y-ave>0となってからの a-aveの総和
found = False;
for t in range(1,len(y)-2):
	if (y[t] > ave[t]):
		acc = acc + (a[t] - ave[t])
	else:
		if found:
			maximulAccel.append(acc)
		found = False
		acc = 0
	if found == False:	#極大発見前なら
		if (y[t] - y[t-1]) > diff and (y[t+1] - y[t]) < -diff:	#極大
	 		if y[t] - ave[t] > 0.5 :	# ある程度大きな値の極大だったら滑りと考える
	 			found = True
	 			maximulTime.append(t)
if found: maximulAccel.append(acc)

#一定値以下の極大値を削除	不要かも
delList = []
for t in range(0,len(maximulAccel)):
	if maximulAccel[t] < 1:	
		delList.append(t)
maximulAccel = [maximulAccel[t] for t in range(0,len(maximulAccel)) if t not in delList]
maximulTime = [maximulTime[t] for t in range(0,len(maximulTime)) if t not in delList]

maximul = [y[t] for t in maximulTime]

#プロット
plt.plot(a)
plt.plot(y)
plt.plot(ave)
plt.plot(maximulTime, maximulAccel, marker='.', markersize=10, linestyle='None')

plt.show()
