/*
*  Copyright (c) 2003-2012, Shoichi Hasegawa and Springhead development team
*  All rights reserved.
*  This software is free software. You can freely use, distribute and modify this
*  software. Please deal with this software under one of the following licenses:
*  This license itself, Boost Software License, The MIT License, The BSD License.
*/

#include "FingerSimulation.h"
#include <conio.h>
#include <string>
#include <algorithm>
#include <cstdio>


using namespace Spr;

std::string MATERIAL = "Acril";

FingerFrictionSimulation app;

int _cdecl main(int argc, char* argv[]) {
	app.ReadData();
	app.Init(argc,argv);
	app.StartSimulation();
	app.Run();
	//app.StartMainLoop();
	return 0;
}


void FingerFrictionSimulation::Init(int argc, char* argv[]) {
	FWApp::Init(argc, argv);							// アプリケーションの初期化
	InitInterface();									// インタフェースの初期化
	BuildScene();										// オブジェクトの作成
}


void FingerFrictionSimulation::StartSimulation(){
	PHHapticEngineIf* he = phscene->GetHapticEngine();	// 力覚エンジンをとってくる
	he->Enable(true);						            // 力覚エンジンの有効化

#if 0
														// シングルスレッドモード
	he->SetHapticStepMode(PHHapticEngineDesc::SINGLE_THREAD);
	phscene->SetTimeStep(hdt);
#elif 1
														// マルチスレッドモード
	he->SetHapticStepMode(PHHapticEngineDesc::MULTI_THREAD);
	phscene->SetTimeStep(pdt);
#else
														// 局所シミュレーションモード
	he->SetHapticStepMode(PHHapticEngineDesc::LOCAL_DYNAMICS);
	phscene->SetTimeStep(pdt);
#endif

	physicsTimerID = GetTimer(0)->GetID();					// 物理スレッドのタイマIDの取得
	GetTimer(0)->SetMode(UTTimerIf::IDLE);					// 物理スレッドのタイマをIDLEモードに設定
#if 1
	UTTimerIf* timer = CreateTimer(UTTimerIf::MULTIMEDIA);	// 力覚スレッド用のマルチメディアタイマを作成
	timer->SetResolution(1);			// 分解能(ms)
	timer->SetInterval(unsigned int(hdt * 5000));		// 刻み(ms)h
	hapticTimerID = timer->GetID();		// 力覚スレッドのタイマIDの取得
	//timer->Start();						// タイマスタート
#else
	UTTimerIf* timer = CreateTimer(UTTimerIf::THREAD);	// 力覚スレッド用のマルチメディアタイマを作成
	timer->SetResolution(1);			// 分解能(ms)
	timer->SetInterval(unsigned int(hdt * 1000));		// 刻み(ms)h
	hapticTimerID = timer->GetID();		// 力覚スレッドのタイマIDの取得
	timer->Start();						// タイマスタート
#endif
	std::cout << ">";	//プロンプト
}



bool MyConsoleDebugMonitor::ProcessKey(int key) {
	if (key == 0x1B) {
		app->Keyboard(key, 0, 0);
		return true;
	}
	else {
		return FWConsoleDebugMonitor::ProcessKey(key);
	}
}

void MyConsoleDebugMonitor::Candidates(std::vector<std::string>& rv, size_t fieldStart, std::string field) {
	UTTypeDescIf* td = UTTypeDescIf::FindTypeDesc("PHMaterial", "Physics");
	CandidatesForDesc(rv, td, field);
}
void MyConsoleDebugMonitor::ExecCommand(std::string cmd, std::string arg, std::vector<std::string> args) {
	UTTypeDescIf* td = UTTypeDescIf::FindTypeDesc("PHMaterial", "Physics");	//	CollisionもPhysicsに含まれる
	PHMaterial m = app->pointer->GetShape(0)->GetMaterial();				//	ポインタのMaterialを取得
	if (ExecCommandForDesc(td, &m, cmd, arg, args) == FWConsoleDebugMonitor::WRITE) {
		//	摩擦係数を更新
		app->UpdateMaterial(m);
	}
}

std::ostream& operator << (std::ostream&os, const LogFrame& hr) {
	os << hr.time << "\t" << hr.contactCount << "\t" << hr.proxyAccel
		<< std::endl;
	return os;
}

FingerFrictionSimulation::FingerFrictionSimulation() :console(this) {
	//	_crtBreakAlloc = 82325;
	pdt = 0.008f;
	hdt = 0.0002f;
	//pdt = 0.04f;
	//hdt = 0.001f;
	hapticCount = 0;
	vcount = 0;
	loopCount = 0;
	bPause = true;
	bSimulation = false;
}

//CMA-ES
void FingerFrictionSimulation::Run() {
	if (NTimers() > 1) {
		GetTimer(0)->Stop(); //物理タイマー停止
		GetTimer(1)->Stop(); //力覚レンダリングタイマーを停止
	}
	//Default lambda = 6
	UTCMAESOptimizerDesc desc;

	desc.lambda = 20;
	desc.cs = 0.01;

	// --
	// CMAESのパラメータを変更したい場合はここでdescにセットする
	// --

	// 最適化クラスの作成
	optimizer = DBG_NEW UTCMAESOptimizer(desc);

	// 次元のセット
	optimizer->SetDimension(7);

	// 初期解のセット
	//double initialValue[] = { 0.5, 0.1, 1800.0, 0.5, 3000.0, 2000.0, 0.001, -20.0, 90.0, 200.0 };
	double initialValue[] = { 0.6, 0.1, 1800.0, 0.5, 3000.0, 2000.0, 0.001};
	//double initialValue[] = { 0,0,0,0,0,0,0,0,0,0 };
	SetMaterial(initialValue);
	optimizer->SetInitialValue(initialValue);


	//SetMaterial(); //パラメータをセット

	// 初期標準偏差のセット
	//double initialStdDev[] = { 0.01, 0.01, 100, 0.1, 100, 100, 0.0001, 1, 10,10 };
	double initialStdDev[] = { 0.1, 0.01, 100, 0.1, 100, 100, 0.0001};

	optimizer->SetInitialStdDev(initialStdDev);

	// 初期化を実行
	optimizer->Initialize();
	DSTR << optimizer->GetCs() << std::endl;
	double hdtMax = pdt / hdt;
	double pdtMax = fingervelocity.size() / hdtMax;
	std::cout << hdtMax << ":" << pdtMax << std::endl;
	// 最適化ループ：最適化が完了するまで続ける
	while (!optimizer->IsFinished()) {
		// 次に目的関数を計算すべきパラメータを取得
		double* population = optimizer->GetPopulation();

		SetMaterial(population);
		double objectiveFunctionValue;
		
		//if (population[0] <= 0 || population[1] <= 0 || population[2] <= 0 || population[3] <= 0 || population[4] <= 0 || population[5] <= 0 || population[6] <= 0 || population[7] >= 0 || population[8] <= 0 || population[9] <= 0) {
		if (population[0] <= 0 || population[1] <= 0 || population[2] <= 0 || population[3] <= 0 || population[4] <= 0 || population[5] <= 0 || population[6] <= 0) {
			objectiveFunctionValue = 100000;
		}
		else {
			if (NTimers() > 1) {
				for (int i = 0; i < (int)pdtMax; ++i) {
					for (int t = 0; t < (int)hdtMax; ++t) {
						GetTimer(1)->Call();
					}
					//	DSTR << "count:" << i << std::endl;
					GetTimer(0)->Call();
				}
			}

			GetSlipPoint();

			// 目的関数を計算して結果をセット
			objectiveFunctionValue = ObjectiveFunction();
		}
		optimizer->SetObjectiveFunctionValue(objectiveFunctionValue);
		//std::cout << objectiveFunctionValue << std::endl;
		// 次へ
		optimizer->Next();
		//DSTR << optimizer->GetCurrentPopulation() << std::endl;
		if (optimizer->GetCurrentPopulation() == 0) {
			// １世代ごとに途中経過表示
			DSTR << "Generation: " << optimizer->GetCurrentGeneration() << std::endl;
			DSTR << "Fitness: " << optimizer->GetFitness() << std::endl;
			DSTR << " -- " << std::endl;
		}
	}

	// 最適化が完了：結果を取得
	double* result = optimizer->GetResult();

	// 結果表示
	DSTR << "Result : " << std::endl;
	for (int i = 0; i < optimizer->GetDimension(); ++i) {
		DSTR << i << " : " << result[i] << std::endl;
	}
	DSTR << " --- " << std::endl;

	SetMaterial(result);
	if (NTimers() > 1) {
		for (int i = 0; i < (int)pdtMax; ++i) {
			for (int t = 0; t < (int)hdtMax; ++t) {
				GetTimer(1)->Call();
			}
			//	DSTR << "count:" << i << std::endl;
			GetTimer(0)->Call();
		}
	}

	time_t now = time(NULL);
	struct tm pnow;
	localtime_s(&pnow, &now);
	std::ostringstream sout;
	sout << MATERIAL << "_Accellog_" << 1900 + pnow.tm_year << "_"
		<< std::setfill('0') << std::setw(2) << pnow.tm_mon
		<< std::setfill('0') << std::setw(2) << pnow.tm_mday << "_"
		<< std::setfill('0') << std::setw(2) << pnow.tm_hour
		<< std::setfill('0') << std::setw(2) << pnow.tm_min << ".txt";
	std::string fileName = sout.str();
	log.open(fileName, std::ios::out);
	char acc[1024];
	for (int i = 0; i < accLog.size(); i++) {
		sprintf_s(acc, sizeof(acc), "%lf", accLog[i]);
		log << acc << std::endl;
	}
	log.close();
	std::ostringstream sout2;
	sout2 << MATERIAL << "_AccelParameter_" << 1900 + pnow.tm_year << "_"
		<< std::setfill('0') << std::setw(2) << pnow.tm_mon
		<< std::setfill('0') << std::setw(2) << pnow.tm_mday << "_"
		<< std::setfill('0') << std::setw(2) << pnow.tm_hour
		<< std::setfill('0') << std::setw(2) << pnow.tm_min << ".txt";
	fileName = sout2.str();
	log.open(fileName, std::ios::out);
	for (int i = 0; i < optimizer->GetDimension(); i++) {
		log << result[i] << std::endl;
	}
	log.close();

	GetSlipPoint();
	double objectiveFunctionValue = ObjectiveFunction();
	DSTR << "ObjectiveFunction:" << objectiveFunctionValue << std::endl;

}

// 目的関数
double FingerFrictionSimulation::ObjectiveFunction() {
	double diffAcc = CompareAcc();
//	DSTR << "pose:" << pointer->GetPose() << std::endl;
//	DSTR << "velocity:" << pointer->GetVelocity() << std::endl;

	accLog.clear();
	accData.clear();
	vcount = 0;
	hapticCount = 0;
	loopCount++;
	PHHapticEngineIf* he = phscene->GetHapticEngine();

	Vec3f lastProxyVel = he->GetPointerInHaptic(0)->GetProxyVelocity().v();
	lastProxyVel.clear();
	pointer->SetDefaultPose(Posed());
	pointer->SetVelocity(Vec3d());
	Posed pose = Posed(Vec3d(0, -fingerforce[0] / pointer->GetReflexSpring(), 0), Quaterniond::Rot(Rad(0), 'z'));
	SpatialVector vel = SpatialVector(Vec3d(0,0,0),Vec3d(0,0,0));
	FWSceneIf* scene = GetSdk()->GetScene();
	FWHapticPointerIf* fp = scene->GetHapticPointer(0);
	fp->GetPHHapticPointer()->UpdateHumanInterface(pose, vel);
	//DSTR << "v:" << pointer->GetProxyVelocity().v() << std::endl;
	//pointer->SetProxyVelocity(SpatialVector());
	return diffAcc;
}

double FingerFrictionSimulation::CompareAcc() {
	//accDataとfingeraccdataを比較して差(評価値)を出力
	double totalaccDiff = 0;
	int countDiff = 0,totalaccIDDiff = 0;
	if (accData.size() == 0) {
//		DSTR << "StickSlipcount:" << countDiff << std::endl;
//		DSTR << "AccDiff:" << totalaccDiff << std::endl;
//		DSTR << "AccIDDiff:" << totalaccIDDiff << std::endl;
		return 100000;
	}
	int totalproxyMaxID = 0,totalfingerMaxID = 0;
	double totalproxyMax = -1, totalfingerMax = -1;
	//最大値のIDをあわせるため
	for (int i = 0; i < accData.size();i++) {
		if (accData[i].maxAcc > totalproxyMax) {
			totalproxyMax = accData[i].maxAcc;
			totalproxyMaxID = i;
		}
	}
	for (int i = 0; i < fingeracc.size(); i++) {
		if (fingeracc[i].maxAcc > totalfingerMax) {
			totalfingerMax = fingeracc[i].maxAcc;
			totalfingerMaxID = i;
		}
	}
	int diffID = totalfingerMaxID - totalproxyMaxID;
	//	最大値から見ていって、差を加算
	for (int i = totalproxyMaxID;; i++) {
		int accIDDiff;
		double accDiff;
		if (i + diffID < fingeracc.size() && i < accData.size()) {
			accDiff = fabs(accData[i].maxAcc - fingeracc[i + diffID].maxAcc);
			accIDDiff = abs((accData[i].maxID - accData[totalproxyMaxID].maxID) - (fingeracc[i + diffID].maxID - fingeracc[totalfingerMaxID].maxID));
			totalaccDiff += accDiff;
			totalaccIDDiff += accIDDiff;
		}
		else if (i + diffID < fingeracc.size()) {
			totalaccDiff += fingeracc[i + diffID].maxAcc;
		}
		else if (i < accData.size()) {
			totalaccDiff += accData[i].maxAcc;
		}
		else break;
	}
	for (int i = totalproxyMaxID-1;; i--) {
		int accIDDiff;
		double accDiff;
		if (i >= 0 && i + diffID >= 0){
			accDiff = fabs(accData[i].maxAcc - fingeracc[i + diffID].maxAcc);
			accIDDiff = abs((accData[i].maxID - accData[totalproxyMaxID].maxID) - (fingeracc[i + diffID].maxID - fingeracc[totalfingerMaxID].maxID));
			totalaccDiff += accDiff;
			totalaccIDDiff += accIDDiff;
		}
		else if (i+diffID >= 0) {
			totalaccDiff += fingeracc[i].maxAcc;
		}
		else if (i >= 0) {
			totalaccDiff += accData[i].maxAcc;
		}
		else {
			break;
		}
	}
	countDiff = abs((int)accData.size() - (int)fingeracc.size());
	DSTR << accData.size() << std::endl;
	DSTR << "StickSlipcount:" << countDiff << std::endl;
	DSTR << "AccDiff:" << totalaccDiff << std::endl;
	DSTR << "AccIDDiff:" << totalaccIDDiff << std::endl;

	return countDiff+totalaccIDDiff+totalaccDiff*10;
}

void FingerFrictionSimulation::SetMaterial(double* population) {
	//for (int i = 0; i < NTimers(); i++)	GetTimer(i)->Stop();
	PHMaterial m = pointer->GetShape(0)->GetMaterial();
	m.mu = population[0];
	m.timeVaryFrictionA = population[1];
	m.timeVaryFrictionB = population[2];
	m.stribeckVelocity = population[3];
	pointer->SetReflexSpring(population[4]);
	pointer->SetFrictionSpring(population[5]);
	pointer->SetFrictionDamper(population[6]);
	//m.vibA = population[7];
	//m.vibB = population[8];
	//m.vibW = population[9];
	UpdateMaterial(m);
	// 全てのタイマを始動
	//for (int i = 0; i < NTimers(); i++)	GetTimer(i)->Start();
}

void FingerFrictionSimulation::GetSlipPoint() {
	std::vector<double> ave;
	ave.resize(accLog.size());
/*	double filter[21] = { -6.21115459e-19, -2.12227115e-03, -6.32535399e-03, -1.16118104e-02,
		 -1.23546567e-02,  4.19252935e-18,  3.17744976e-02,  8.14359076e-02,
		  1.37493782e-01,  1.82125490e-01,  1.99168830e-01,  1.82125490e-01,
		  1.37493782e-01,  8.14359076e-02,  3.17744976e-02,  4.19252935e-18,
		 -1.23546567e-02, -1.16118104e-02, -6.32535399e-03, -2.12227115e-03,
		 -6.21115459e-19
	};*/
	double filter[5] = { 0.1, 0.2, 0.4, 0.2, 0.1};
	const int nTap = sizeof(filter) / sizeof(filter[0]);
	const int startPos = -nTap / 2;
	for (int t = 0; t < ave.size(); ++t) {
		int start = startPos;
		int end = startPos + nTap;
		if (t + start < 0) start = 0 - t;
		if (t + end > ave.size()) end = ave.size() - t;
		ave[t] = 0;
		for (int i = start; i != end; ++i) {
			ave[t] += accLog[t + i] * filter[i+nTap/2];
		}
	}
	accData.clear();
	for (int t = 0; t < ave.size(); ++t) {
		const double th = 1;
		if (ave[t] > 0 && accLog[t] - ave[t] > th
			|| ave[t] < 0 && accLog[t] - ave[t] < -th){
			AccelFrame af;
			af.maxAcc = accLog[t];
			af.maxID = t;
			accData.push_back(af);
		}
	}
}
void FingerFrictionSimulation::ReadData() {
	//速度読み込み
	std::string velocityfileName = "InputData/"+MATERIAL+"/"+MATERIAL+"Velocity.txt";
	std::ifstream fin(velocityfileName); //ファイルを読み込み
	std::string indata;

	if (!fin) std::cout << "ファイルが開けませんでした。" << std::endl;

	while (!fin.eof()){
		std::getline(fin, indata);
		if (indata.length()) {
			fingervelocity.push_back(std::stod(indata));
		}
	}
			
	//抗力読み込み
	std::string forcefileName = "InputData/"+MATERIAL+"/"+MATERIAL+"Force.txt";
	std::ifstream fin2(forcefileName); //ファイルを読み込み

	if (!fin2) std::cout << "ファイルが開けませんでした。" << std::endl;

	while (!fin2.eof()) {
		std::getline(fin2, indata);
		if (indata.length()) {
			fingerforce.push_back(std::stod(indata));
		}
	}

	//加速度読み込み
	std::string accfileName = "InputData/"+MATERIAL+"/"+MATERIAL+"Accel.txt";
	std::ifstream fin3(accfileName); //ファイルを読み込み

	if (!fin3) std::cout << "ファイルが開けませんでした。" << std::endl;

	for (int i = 0; getline(fin3, indata); i++) {
		std::istringstream stream(indata);
		AccelFrame af;
		double data;
		stream >> data;
		af.maxID = (int)data;
		stream >> data;
		af.maxAcc = data;
		fingeracc.push_back(af);
	}
}

void FingerFrictionSimulation::BuildScene() {
	PHSdkIf* phSdk = GetSdk()->GetPHSdk();
	phscene = GetSdk()->GetScene()->GetPHScene();
	phscene->SetContactTolerance(0.0002);

	Vec3d pos = Vec3d(0, 0, 0.1);						// カメラ初期位置
	GetCurrentWin()->GetTrackball()->SetPosition(pos);	// カメラ初期位置の設定
	GetSdk()->SetDebugMode(true);						// デバック表示の有効化
	GetSdk()->GetScene()->EnableRenderHaptic(true);		//	力覚デバッグ表示ON
	GetSdk()->GetScene()->EnableRenderContact(false);	//	接触のデバッグ表示ON

	// 床を作成
	CDBoxDesc bd;
	bd.boxsize = Vec3f(5.0f, 1.0f, 5.0f);

	floor = phscene->CreateSolid();
	floor->AddShape(phSdk->CreateShape(bd));
	floor->SetFramePosition(Vec3d(0, -bd.boxsize.y / 2, 0.0));
	floor->SetDynamical(false);
	floor->SetName("soFloor");

	// 力覚ポインタの作成
	pointer = phscene->CreateHapticPointer();			// 力覚ポインタの作成
	CDSphereDesc cd;									//　半径1cmの球
	cd.radius = 0.01f;
	cd.material = bd.material;
	CDShapeIf* shape = phSdk->CreateShape(cd);			//	どちらかを作る
	shape->SetDensity(0.006f / shape->CalcVolume());	//	指の重さは大体 6g
	pointer->AddShape(shape);	// シェイプの追加
	//pointer->SetShapePose(0, Posed(Vec3d(), Quaterniond::Rot(Rad(10), 'z')));
	pointer->SetDefaultPose(Posed());					//	力覚ポインタ初期姿勢の設定
	pointer->CompInertia();								//	質量と慣性テンソルを密度から計算
	pointer->SetLocalRange(0.02f);						//	局所シミュレーション範囲の設定
	pointer->SetPosScale(1.0f);							//	力覚ポインタの移動スケールの設定
	pointer->SetFrictionSpring(1200.0f);					//	DynamicProxyの際の摩擦計算に使うバネ係数
	pointer->SetReflexSpring(3000.0f);					//	力覚レンダリング用のバネ
	pointer->SetReflexDamper(0.0f);						//	力覚レンダリング用のダンパ
	pointer->SetRotationReflexSpring(30.0f);			//	力覚レンダリング用の回転バネ
	pointer->SetRotationReflexDamper(0.0f);				//	力覚レンダリング用の回転ダンパ
	pointer->SetName("hpPointer");
	pointer->EnableRotation(false);
	pointer->EnableFriction(true);
	pointer->EnableVibration(true);
	pointer->SetHapticRenderMode(PHHapticPointerDesc::DYNAMIC_PROXY);
	pointer->EnableTimeVaryFriction(true);
	pointer->EnableForce(true);

	FWHapticPointerIf* fwPointer = GetSdk()->GetScene()->CreateHapticPointer();	// HumanInterfaceと接続するためのオブジェクトを作成
	fwPointer->SetHumanInterface(device);		// HumanInterfaceの設定
	fwPointer->SetPHHapticPointer(pointer);		// PHHapticPointerIfの設定
	PHHapticEngineIf* he = phscene->GetHapticEngine();
	for (int i = 0; i < he->NSolids(); ++i) {
		if (he->GetSolidPair(i, 0)->GetSolid(0) == floor) {
			floorId = i;
			break;
		}
	}
}

void FingerFrictionSimulation::IdleFunc() {
	console.KeyCheck();
}
void FingerFrictionSimulation::InitInterface() {
	HISdkIf* hiSdk = GetSdk()->GetHISdk();
	//	実デバイスの追加
	//	for SPIDAR
	// x86
	DRUsb20SimpleDesc usbSimpleDesc;
	hiSdk->AddRealDevice(DRUsb20SimpleIf::GetIfInfoStatic(), &usbSimpleDesc);
	DRUsb20Sh4Desc usb20Sh4Desc;
	for (int i = 0; i < 10; ++i) {
		usb20Sh4Desc.channel = i;
		hiSdk->AddRealDevice(DRUsb20Sh4If::GetIfInfoStatic(), &usb20Sh4Desc);
	}
	// x64
	DRCyUsb20Sh4Desc cyDesc;
	for (int i = 0; i < 10; ++i) {
		cyDesc.channel = i;
		hiSdk->AddRealDevice(DRCyUsb20Sh4If::GetIfInfoStatic(), &cyDesc);
	}
	hiSdk->AddRealDevice(DRKeyMouseWin32If::GetIfInfoStatic());

	//	インタフェースの取得
	device = hiSdk->CreateHumanInterface(HISpidarGIf::GetIfInfoStatic())->Cast();
	if (device->Init(&HISpidarGDesc("SpidarG6X3F"))) {
		device->Calibration();
	}
	else {	//	XBOX
		device = hiSdk->CreateHumanInterface(HIXbox360ControllerIf::GetIfInfoStatic())->Cast();
		if (!device->Init(NULL)) {
			device = hiSdk->CreateHumanInterface(HINovintFalconIf::GetIfInfoStatic())->Cast();
			if (!device->Init(NULL)) {
				device = hiSdk->CreateHumanInterface(HIHapticDummyIf::GetIfInfoStatic())->Cast();
				device->Init(NULL);
				dummyDevice = device->Cast();
			}
		}
	}
	hiSdk->Print(DSTR);
	hiSdk->Print(std::cout);
}

void FingerFrictionSimulation::UpdateHapticPointersByData() {
	static Posed pose = Posed(Vec3d(0, -fingerforce[0] / (pointer->GetReflexSpring()), 0), Quaterniond::Rot(Rad(0), 'z'));
	SpatialVector vel = SpatialVector(Vec3d(0,0,0),Vec3d(0,0,0));
	if (hapticCount == 0) {
		pose = Posed(Vec3d(0, 0.01 + 0.001, 0), Quaterniond::Rot(Rad(0), 'z'));
		pointer->SetVelocity(Vec3d(0,0,0));
		pointer->SetProxyVelocity(SpatialVector(Vec3d(0,0,0),Vec3d(0,0,0)));
	}
	if (hapticCount > 0) {
		double depth = fingerforce[vcount] / pointer->GetReflexSpring();
		//std::cout << -depth << std::endl;
		vel = SpatialVector(Vec3d(fingervelocity[vcount], 0, 0), Vec3d());
		pose.y = 0.01 -depth;
		vcount++;
	}
	
	//else vel = SpatialVector(Vec3d(0.0f,-0.0000005f,0.0f), Vec3d());

	//pose.Pos() += vel.v() * phscene->GetTimeStep();
	pose.Pos() += vel.v() * hdt;
	FWSceneIf* scene = GetSdk()->GetScene();
	PHHapticEngineIf* he = phscene->GetHapticEngine();
	if (he->GetHapticStepMode() == PHHapticEngineDesc::SINGLE_THREAD) {
		if (scene) {
			for (int i = 0; i < scene->NHapticPointers(); i++) {
				FWHapticPointerIf* fp = scene->GetHapticPointer(i);
				fp->GetPHHapticPointer()->UpdateHumanInterface(pose, vel);
			}
		}
	}
	else {  // multi thread
		if (!scene || he->NPointersInHaptic() == 0) return;
		for (int i = 0; i < scene->NHapticPointers(); i++) {
			PHHapticPointerIf* plp = (PHHapticPointerIf*)he->GetPointerInHaptic(i);
		//std::cout << scene->NHapticPointers() << std::endl;
			plp->UpdateHumanInterface(pose, vel);
		}
	}
	/*
	if (vcount > fingervelocity.size() - 1) {
		//for (int i = 0; i < NTimers(); i++)	GetTimer(i)->Stop();
		GetSlipPoint();
	}
	*/
}

void FingerFrictionSimulation::TimerFunc(int id) {
	if (phscene->GetHapticEngine()->GetHapticStepMode() == PHHapticEngineDesc::SINGLE_THREAD) {
		if (hapticTimerID == id) {
			GetSdk()->GetScene()->UpdateHapticPointers();
			UpdateHapticPointersByData();
			phscene->Step();
		}
		else if (physicsTimerID == id) {
			PostRedisplay();

		}
	}
	else {	//	multi thread
		if (hapticTimerID == id) {
			//GetSdk()->GetScene()->UpdateHapticPointers();
			UpdateHapticPointersByData();
			phscene->StepHapticLoop();
			PHHapticEngineIf* he = phscene->GetHapticEngine();

			if (he->NPointersInHaptic() && he->NSolidsInHaptic()) {
				PHSolidPairForHapticIf* sp = he->GetSolidPairInHaptic(floorId, 0);
				static Vec3f lastProxyVel;
				//	if (sp->GetFrictionState() != sp->FREE && sp->GetSolid(0)->NShape() && sp->GetSolid(1)->NShape()) {
				if (sp->GetSolid(0)->NShape() && sp->GetSolid(1)->NShape()) {
					PHShapePairForHapticIf* sh = sp->GetShapePair(0, 0);
			
					if (sh->NIrs() - sh->NIrsNormal() > 0) {

					}
					else {
						
					}
					Vec3f proxyVel = he->GetPointerInHaptic(0)->GetProxyVelocity().v();
					static std::ofstream file("proxyvel.txt");
					if (loopCount == 0) {
						file << proxyVel.x << std::endl;
					}
					else {
						file.close();
					}
					double proxyAccel = (proxyVel.x - lastProxyVel.x) / hdt;
					//if (proxyAccel < 0) std::cout << "Accel:" << proxyAccel << std::endl;
					if (fabs(proxyAccel) > 10000) proxyAccel = 0;
					accLog.push_back(proxyAccel);
					lastProxyVel = proxyVel;
				}
				else {
					lastProxyVel.clear();
				}


			}
			phscene->StepHapticSync();
			hapticCount++;
		}
		else {
			PHHapticEngineIf* he = phscene->GetHapticEngine();
	//		if(!bPause)
				he->StepPhysicsSimulation();
			PostRedisplay();
		}
	}
}


void FingerFrictionSimulation::UpdateMaterial(const PHMaterial& m) {
	//	摩擦係数を更新
	pointer->GetShape(0)->SetMaterial(m);
	floor->GetShape(0)->SetMaterial(m);
	//	HapticEnigneの中のShapePairの摩擦係数を更新
	PHHapticEngineIf* he = phscene->GetHapticEngine();
	PHSolidPairForHapticIf* sp = NULL;
	PHShapePairForHapticIf* sh = NULL;
	if (he->NPointers() && he->NSolids()) {
		sp = he->GetSolidPair(floorId, 0);
		//std::cout << sp->GetFrictionState() << std::endl;
		if (sp->GetSolid(0)->NShape() && sp->GetSolid(1)->NShape()) {
			sh = sp->GetShapePair(0, 0);
		}
	}
	if (sh) {
		CDShapeIf* s1 = pointer->GetShape(0);
		CDShapeIf* s2 = sh->GetFrame(1)->GetShape();
		sh->GetFrame(0)->GetShape()->SetMaterial(m);
		sh->GetFrame(1)->GetShape()->SetMaterial(m);
		sh->UpdateCache();
		pointer->GetShape(0)->SetMaterial(m);
		floor->GetShape(0)->SetMaterial(m);
	}
}