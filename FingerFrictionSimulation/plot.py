from matplotlib import pyplot
import math
import numpy as np
import matplotlib.pyplot as plt
import os
import glob


#ファイル読み出し
name = 'Acril'


files = glob.glob("./" + name + "_Accellog*.txt")
if len(files) == 0: quit()

path1 = files[0]
with open(path1) as f:
	s = f.read()
	sim = s.split();
	sim = [float(s) for s in sim]

path2 = 'InputData/' + name  + '/' + name + 'Accel.txt'
print(path2)
with open(path2) as f2:
	s2 = f2.read()
	a = s2.split();
	a = [float(e) for e in a]

time = a[0:len(a):2]
value = a[1:len(a):2]

#プロット
plt.plot(sim)
plt.plot(time, value, marker='.', markersize=10, linestyle='None')

plt.show()
