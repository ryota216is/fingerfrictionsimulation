#ifndef FINGER_SIMULATION_H
#define FINGER_SIMULATION_H

#include <Springhead.h>
#include <Framework/SprFWApp.h>
#include <Framework/SprFWConsoleDebugMonitor.h>
#include <Foundation/UTOptimizer.h>
#include <vector>


using namespace Spr;

class FingerFrictionSimulation;

class MyConsoleDebugMonitor : public FWConsoleDebugMonitor {
	virtual void ExecCommand(std::string cmd, std::string arg, std::vector<std::string> args);
	virtual void Candidates(std::vector<std::string>& rv, size_t fieldStart, std::string field);
	virtual bool ProcessKey(int key);
	FingerFrictionSimulation* app;
public:
	MyConsoleDebugMonitor(FingerFrictionSimulation* a) :app(a) {}
};

struct LogFrame {
	double time;
	double proxyAccel;
	unsigned contactCount;
};

struct AccelFrame {
	double maxAcc;	//	Slip時の(最大)加速度
	int maxID;	//	フレーム番号
};

struct MaterialData {
	float mu;
	float timeVaryA;
	float timeVaryB;
	float rSpring;
	float fSpring;
	float fDamper;
	float vibA;
	float vibB;
};

class FingerFrictionSimulation : public FWApp {
public:
	MyConsoleDebugMonitor console;
	PHSceneIf* phscene;			// PHSceneへのポインタ
	PHHapticPointerIf* pointer; // 力覚ポインタへのポインタ
	PHSolidIf* floor;			// 床
	int floorId;
	float pdt;					// 物理スレッドの刻み
	float hdt;					// 力覚スレッドの刻み
	int physicsTimerID;			// 物理スレッドのタイマ
	int hapticTimerID;			// 力覚スレッドのタイマ
	UTRef<HIBaseIf> device;		// 力覚インタフェースへのポインタ
	HIHapticDummyIf* dummyDevice;	// キーボードで操作するダミーデバイス
	bool bPause;				//	シミュレーションの一時停止
	bool bSimulation;

	RingBuffer<double> logRing;	//	実験の結果結果
	std::vector<double> accLog; //シミュレーションの加速度の実験結果
	int hapticCount;
	int loopCount;
	std::ofstream log;			//	ログファイル

	FingerFrictionSimulation();
	std::vector<double> fingervelocity; //速度データ
	std::vector<AccelFrame> fingeracc; // 加速度データ
	std::vector<double> fingerforce; //抗力データ
	std::vector<AccelFrame> accData; //必要な加速度データ
	UTCMAESOptimizer* optimizer;
	void Run();
	double ObjectiveFunction();

	void ReadData();
	int vcount = 0;
	void GetSlipPoint();
	double CompareAcc();
	void SetMaterial(double* population);  //　パラメータの設定
	void Init(int argc = 0, char* argv[] = 0);	// アプリケーションの初期化
	void InitInterface();
	virtual void BuildScene();						// オブジェクトの作成
	void TimerFunc(int id);							// 各タイマが呼ぶコールバック関数
	void UpdateMaterial(const PHMaterial& m);
	void IdleFunc();
	void UpdateHapticPointersByData();
	void StartSimulation();

};


#endif